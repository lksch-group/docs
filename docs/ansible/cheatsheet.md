---
title: ansible cheatsheet
tags:
  - ansible
  - cheatsheet
---

{% extends 'page_base.md' %}
{% block content %}

# Ansible cheatsheet

## ansible-doc

Every module's doc can be accessed via cli, no need to visit the website:

    ansible-doc copy

## ansible-console

Ansible console allows to **connect and run commands on multiple hosts** at the 
same time:

    ansible-console -l subset

## ansible-inventory

**Graph all hosts** of a (dynamic) inventory:

    ansible-inventory -i inventory/hosts.yml --graph

## debug module

**List groups** of all servers:

     ansible all -m debug -a "var=group_names"

Check if vars set on a group name is accessible by it's members. Let's check
if the `EDITOR` variable is set for all workstations:

    ansible workstations -m debug -a "var=EDITOR"

## ping module

**Ping** a group of hosts:

    ansible servers -m ping

## setup module

Sometimes a playbook fails with `ansible_host` is not defined, even though `gather_facts: true` is set in the playbook. 
In this case, you can force to **gather facts** using the setup module:

    ansible <inventory_name> -m setup

## ansible-playbook

Use a **subset of hosts**:

    ansible-playbook -l subset playbook.yml

Use **another inventory**:

    ansible-playbook -i inventory/production.yml playbook.yml

Check **which hosts will be targeted** by a play:

    ansible-playbook --list-hosts backup.yml

## ansible.cfg example

Example `ansible.cfg` for an ansible repo specifying the **default inventory** and **fact cache location**.
{{ macros.file_include('examples/ansible/ansible.cfg', lang='ini') }}

## Vagrantfile example

Here is an example **Vagrantfile to test playbooks** which includes 
the option of setting the groups of these hosts:

{{ macros.file_include('examples/vagrant/Vagrantfile_ansible', lang='ruby') }}

## Testing with vagrant

The playbooks are registered inside the `Vagrantfile` and used provisioning the machine.

    vagrant up debianbuster

Rerun the playbook:

    vagrant provision

Enter the machine:

    vagrant ssh

{% endblock %}
