---
title: Community Gallery
description: Linksammlung als Gallerie
hide:
  - navigation
  - toc
filters:
  - name: Tags
    attr: tags
    type: multi-select

podcasts:
  - name: "SRF Digital"
    url: https://www.srf.ch/audio/digital-podcast
    img_url: ""
    tags: ['Gaming', 'Digitalisierung', 'Kunst', 'News']
    lang: de
    order: 1
  - name: "Destination Linux"
    url: '#'
    img_url: ''
    tags: ['Linux', 'Audio&Video', 'Trends', 'FOSS']
    lang: en
    order: 2
  - name: "Darknet Diaries"
    url: 'https://darknetdiaries.com/'
    img_url: ''
    tags: ['Stories', 'Unterhaltung', 'IT Security']
    lang: en
    order: 2

news_sites:
  - name: heise online
    url: 'https://www.heise.de/'
    img_url: ''
    lang: de
    tags: ['News', 'IT', 'Media']
    order: 1.9
  - name: Republik
    url: 'https://www.republik.ch/'
    img_url: ''
    lang: de
    tags: ['News', 'Verein']
    order: 1

vereine:
  - name: Digitale Gesellschaft
    url: 'https://www.digitale-gesellschaft.ch/'
    img_url: ''
    lang: de
    tags: ['IT', 'Verein', 'Konsumenten­schutz']
    order: 1

projekte:
  - name: Flash Drives for Freedom
    url: https://flashdrivesforfreedom.org/
    lang: en
    tags: ['Aktivismus', 'Demokratie']
---

{% set items = podcasts + news_sites + vereine %}
{% set flex_basis = '16%' %}
{% include "gallery.html" %}
