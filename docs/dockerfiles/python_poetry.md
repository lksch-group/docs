---
title: Python + Poetry
tags:
  - Docker
  - Poetry
---
{% extends 'page_base.md' %}
{% block content %}

# Intro

Poetry is my goto build tool now, but I found it hard to get a simple and working docker image for different scenarios. 
Most of the time I will have some console scripts in my python package, and these console scripts are also available when 
running `poetry install` on my developer machine. My needs were the following:

- Poetry installed for development incl. python console scripts (entrypoints)
- additional user `dev` to run the docker images having the same UID as the developer (assuming 1000)
- Creating production build using `poetry build` command for a slimmer final image
- Proper separation between poetry related folders and the project folder (`$PWD` locally, `/app` in docker)

It took me some time to figure out everything, so I'd like to share my learnings here.

## Python base image

The base image does **not yet contain poetry, but all system dependencies** that your final application requires. 
Consider poetry to be a build tool, whereas the python container with python itself is enough to run a python package 
built by poetry. This stage is later used by the production stage, where only the built package is copied from one stage 
and then installed.

??? warning "Lacking docker `COPY` options for Windows users"

    The command `COPY --chown` is not supported on Windows. This is used in the examples below. As workaround, I guess you
    can solve the problem by using `RUN chown -R dev:dev ...` inside the Dockerfile after the `COPY` operations.

{{ macros.file_include('examples/dockerfiles/poetry/python_base', lang='dockerfile') }}

**Explanations of environment variables**

- `PYTHONUNBUFFERED`: Ensures output streams are straight und unbuffered to the terminal
- `PYTHONDONTWRITEBYTECODE`: Will prevent writing `*.pyc` files which are binary files created upon first run of the program. Does make sense if the main python process does not spawn further processes.
- `PIP_NO_CACHE_DIR`: Disables pip cache as in `pip install --no-cache-dir ...` resulting in smaller images
- `POETRY_VIRTUALENVS_IN_PROJECT`: Tell poetry not to create a `.venv` inside `/app`. On your developer machine, it is then best to set `poetry config virtualenvs.in-project` to `false` as well.

## Development build

You would build this stage for development, additionally mounting `$PWD` to `/app` e.g. to detect file changes when developing. 
The development stage does:

- Install `poetry` respecting the `POETRY_*` environment variables during install
- The `pyproject.toml` and the `poetry.lock` are copied
- Installing package dependencies (incl. dev ones) using `poetry install`
- Copy the entire source code `COPY . .` using user `dev` and workdir `/app`
- Optional: `RUN poetry install` under user `dev` a second time to install entrypoint scripts

{{ macros.file_include('examples/dockerfiles/poetry/stage_development', lang='dockerfile') }}

**Note**  

- Use `poetry run ...` in order to activate the virtual environment
- poetry itself as well as requirements are installed as user `dev`

??? question "Should I use a .dockerignore to exclude sensible files being built into the image?"

    When using a multistage build like this, we might build files into the development container that are not necessary in production. 
    This is fine as long as we make sure the final stage doesn't contain (sensible) developer files built into the container (:fire:caution with `COPY . .`). 
    So you can do without a [`.dockerignore`]({{ urls.docker_ignore }}) file as well.

??? info "Making entrypoints available"
    If you have console scripts in your `pyproject.toml`, your entrypoints (in `/opt/.cache/virtualenvs/.../bin`) will 
    not be created unless the source code of your package is available. This is the reason why we can get away with a 
    small hack using `poetry install` after `COPY . .` a second time, but this time around requires minimal time to perform the step.

### Example docker-compose

A `docker-compose` file may look like this (replace `package_name` with the name of your package 
according to `pyproject.toml`:

{{ macros.file_include('examples/dockerfiles/poetry/docker-compose.yml', lang='yaml') }}

## Production build

Here we use two stages actually for the reason of **getting rid of poetry in the final image**. 
We use the development stage that has poetry and the source code. We build the package first, and in 
the **second stage we just copy sdist package** and install it into the container itself.

{{ macros.file_include('examples/dockerfiles/poetry/stage_production', lang='dockerfile') }}

## Complete example

Here is the full `Dockerfile` consisting of all the stages above:

```dockerfile
--8<-- "examples/dockerfiles/poetry/python_base"

--8<-- "examples/dockerfiles/poetry/stage_development"

--8<-- "examples/dockerfiles/poetry/stage_production"
```

## Example build script

Here is an example build script that takes advantage of the info in `pyproject.toml` 
regarding package name and version for tagging. It shows the usage of the build args used in the `Dockerfile` above.

{{ macros.file_include('examples/scripts/poetry_build.sh', lang='bash') }}

**Usage:**

```
# For development build
./build.sh
# indication other stage
./build.sh production
```

{% endblock %}