---
title: Nextcloud mit NextcloudPi
description: Erfahrungsbericht Installation Nextcloud mit NextcloudPi Image
---
## Installation mit Raspi und NextcloudPi Image

Kurzer Erfahrungbericht. Es wurde folgendes verwendet:

- Raspi4 mit 8GB

- externe Festplatte

NextcloudPi hat ein offizielles Image, das man auf die SD flashen kann. Dieses hat beim Raspi4 aber nicht gestartet. Danach haben wir ein **Raspian Slim** Image benutzt, und **Nextcloud Pi mittels Script** installiert, was funktionierte.

Das Raspi sollte auch für den [Fernzugriff mit ssh abgesichert](/../raspi/initial_config/) werden. 

Offizielle Docs für das Image [hier](https://docs.nextcloudpi.com/en/how-to-install-nextcloudpi/).
Alle Installationsvarianten (Docker etc) [hier](https://ownyourbits.com/nextcloudpi/#content_start).
