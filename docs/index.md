---
title: Über
hide:
  - toc
  - navigation
---
# ![RD Logo](img/icon_twister_black.svg) Projekt Real | Digital

**Real | Digital** wurde einmal als Name erdacht und dann als gelegentlich aktualiserte Website weitegeführt. Ich hoffe, dass die Infos auf dieser Website der einen oder anderen Person nützlich sind.

Falls du **kein super neues Gerät oder keine Lust auf das neue [Windows 11](./opensource/linux_vs_win/win11.md)** hast, schau dir [Linux v.s. Windows](./opensource/linux_vs_win/index.md) an.

{% for item in follow_up %}
{% include 'fw_button.md' %}
{% endfor %}