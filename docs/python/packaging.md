---
title: Python and packaging of software
tags:
  - Python
  - Packaging
---

## What is a python package?

A python package refers to a software bundle that contains python code with it's dependencies. A python package can be 
built and uploaded to [PyPi]({{ urls.pypi }}) and then installed using `pip install`. Distributed python packages support 
different python versions or operating systems.

## How to create a python package?

!!! tip
    You can use cookiecutter templates to bootstrap new projects. For python, take a look at [cookiecutter-pypackage]({{ urls.cookiecutter_pypackage }}).

## How can I install my own python packages?

To start, you can install your python package from git repository directly.

    pip --user git+https://gitlab.com/user/project.git#main

For a private repo

    pip install --user git+ssh://git@gitlab.com/user/project.git#main


!!! warning

    You might want to use [pipx]({{ urls.pipx }}) instead of `pip install --user` if you want to install a global package like a cli in order to 
    prevent dependency conflicts with installed system packages. 

!!! tip

    If you are building **docker images** in projects that depend on one of your private repos, you could mount an ssh_key during build 
    to access the repository.

## Reference

- [Install packages]({{ urls.python_packaging_install_pkgs }})
- [Python packaging guide]({{ urls.python_packaging_homepage }})


{% include 'glossary.md' %}
