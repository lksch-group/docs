---
title: Patterns to adapt
---

# Patterns to adopt

**:white_check_mark: Handle cases with less logic first**

Handle cases where you can return something first, all the code that follows does not 
need to be indented and the code is clearer.

```python
--8<-- "examples/handle_unwanted_first.py"
```