---
title: poetry cheatsheet
tags:
  - Python
  - Poetry
pros:
- "Easier to add, remove and update dependencies"
- "packages are resolved accurately"
- "package hashes of every version are tracked in a `package.lock`"

cons:
- "`package.lock` json leads to merge conflicts in feature branches"
---

{% extends 'page_base.md' %}
{% block content %}

Install package from an existing project (having a `pyproject.toml`)

    poetry install

Activate/create an environment:

    poetry shell

Add new requirement:

    poetry add other_package

Add new **dev requirement**

    poetry add -D pytest_asyncio

**Update** requirements

    poetry update

## Solving Errors

**'Link' object has not attribute 'is_absolute'**:
    - try `rm -rf $HOME/.cache/pypoetry/artifacts/* && rm poetry.lock && poetry install`

## Using poetry as package manager

{{ macros.pros_cons(pros, cons) }}

{% endblock %}