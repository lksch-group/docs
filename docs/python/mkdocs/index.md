---
title: mkdocs development
tags:
  - mkdocs
  - static site
  - development
---

{% extends 'page_base.md' %}

{% block content %}

# mkdocs

Following a quick rundown of how to configure the mkdocs project structure using **macros**, **includes**, a **base page** and 
**external data files** to better organize the data for our content.

## Installation
We need the python packages `mkdocs` / `mkdocs-material` and `mkdocs-macros-plugin`.

=== "Using poetry"
    ```shell
    poetry init --dependency mkdocs-material mkdocs-macros-plugin
    ```

=== "Using pip"
    ```shell
    cd mydocs
    python -m venv venv
    source venv/bin activate
    pip install mkdocs-material mkdocs-macros-plugin
    ```

The **mkdocs cli** features a command to create a new project:

```shell
mkdocs new mydocs
```

## Snippets

The **mkdocs-macros** plugin allows to add **includes** as `*.md` or `*.html`. Here snippets is next do the `docs_dir`, which is 
`docs` by default.

```yaml
plugins:
  - search:
      lang: en
  - macros:
      include_dir: snippets
```

## Glossary

In tech, we love cryptic abbreviations, right :sweat_smile:? So to add a glossary, 
place a file `glossary.md` into the `snippets` directory:

``` markdown title="snippets/glossary.md"
*[OS]: Operating System (Betriebssystem)
```

At the bottom of your page, you can **include the glossary** like in this page example:

``` markdown
---
title: My title
---

My preferred OS is a Linux.

{% raw %}{% include 'glossary.md' %}{% endraw %}
```

## Macros

Macros are a little different to includes. They are more like functions, but can not return data, only text. 
To add a macros file, add a `macros.md` to `snippets` directory:

``` markdown title="snippts/macros.md"
{% raw %}
{%- macro linkify(text) -%}
{% set t = text | string %}
{%- if t.startswith('https://') or t.startswith('http://') -%}
[{{ t }}]({{ t }})
{%- else -%}
{{ t }}
{%- endif -%}
{%- endmacro -%}
{% endraw %}
```

In order to use the macros on a page, you have to include it in the page: 

``` markdown
{% raw %}{% import 'macros.md' as macros with context %}

Linkified link: {{ macros.linkify('https://www.python.org/') }}
{% endraw %}
```

See also the docs of the [mkdocs-macros-plugin]({{ urls.mkdocs_macros }}), which you can use to add your custom plugins 
in order to build your custom workflows.

## Adding a base page

In order to achieve consistent results and **make macros on each page available**, you can add a base page that contains 
the macros.

``` markdown title="snippets/page_base.md"
{% raw %}
{% import 'macros.md' as macros with context %}

{% block content %}
{% endblock %}

{% include 'glossary.md' %}
{% endraw %}
```

Reuse the `base_page.md` using the `extends` directive:
``` markdown
{% raw %}
---
title: Mypage
---
{% extends 'page_base.md' %}
{% block content %}
# Page Title
{% endblock %}
{% endraw %}
```

## Drive documentation with data files

Instead of adding variables to the `extra` key in `mkdocs.yml`, it might be useful to have dedicated data files that can 
be included using the `mkdocs-macros` plugin. For example, you might create a `urls.yml` file with all 
urls used in your project. It makes sense to have them in a central place and to reference them from there.

``` yml title="mkdocs.yml"
plugins:
  - search:
      lang: de
  - macros:
      include_dir: snippets
      include_yaml:
        - urls: data/urls.yml
```

``` yaml title="data/urls.yml"
github: 'https://github.com'
```

You can then reference the url by a name: `{% raw %}{{ urls.github }}{% endraw %}`. 
It is also **possible to use relative paths** like `../otherrepo/data/hosts.yml` to fetch data from outside the git repo.

!!! note "Re-render when data changes"
    
    By default, mkdocs does not watch for changes in the `data` directory. You can tell it to do so by using
    `mkdocs serve --watch data`.

## Making drafts

I found that I like to add markdown pages as drafts and commit them. I made sure not to include them in the `nav` in `mkdcos.yml`. 
In that case, the draft page can be found using the search because it will be rendered as html. So there are other possibilities depending 
on your workflow

- Pushing only to `main`, making **draft visible for others**, but **ignored for html rendering**: Put a `.` in front of the filename.
- Pushing only to `main`, making **draft visible for others**, **discoverable by search**: Just exclude from nav.
- Using **feature branch**: **not really visible to others**, normal workflow where you include the page in the nav where desired

## Builtin filters

Filtering, ordering or modifying data using Jinja2 filters is often needed. You can include the code below on your 
page to help you find all available filters. 

```markdown
{% raw %}{% for filter in filters_builtin %}`{{ filter }}`, {% endfor %}{% endraw %}
```
Result: 

{% for filter in filters_builtin %}`{{ filter }}`, {% endfor %}

## How this doc site is made

Following the up-to-date configurations that are used to build this site.

{{ macros.file_include('Dockerfile', lang='dockerfile') }}

To automatically deploy the generated website using a Gitlab CICD, I use the [jwilder/nginx-proxy]({{ urls.jwilder_proxy }}) 
in combination with a `docker-compose.yml`.

{{ macros.file_include('docker-compose.yml') }}

And the CICD configuration using Gitlab with a small helper script:

{{ macros.file_include('build.sh') }}
{{ macros.file_include('.gitlab-ci.yml') }}

## Examples

### Open external links in new tabs using javascript

Add this javascript code to `docs/js/custom.js` and add it to `mkdocs.yml`:

```yaml
extra_javascript:
  - js/custom.js
```

{{ macros.file_include('examples/mkdocs/external_links_new_window.js', lang='js') }}

### Table Macro

If you want to generate a tables from data (list of lists), you can use the following macro, that also uses the 
macro `linkify` mentioned above:

```markdown title="snippets/macros.md"
{% raw %}
{%- macro table_header(headers) %}
| {{ headers | join (' | ')}} |  
| {% for i in headers %} - |{% endfor -%}
{%- endmacro -%}

{%- macro table_row(row_data, row_macro=None) -%}
| {%- for item in row_data %} {{ linkify(item) }} | {% endfor %}
{%- endmacro %}

{%- macro table(headers, data) %}
| {{ headers | join (' | ')}} |  
| {% for i in headers %} - |{% endfor %}  
{% for line in data -%}
| {%- for row in line %} {{ linkify(row) }} | {% endfor %}
{% endfor %}
{%- endmacro %}
{% endraw %}
```

**Note**: The line and whitespace control using `-` in Jinja2 directives is important here to get the right formatting.

{% endblock %}
