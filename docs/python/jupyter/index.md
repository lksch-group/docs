---
title: Jupyter Cheatsheet
---

{% extends 'page_base.md' %}
{% block content %}

# Jupyter Notebooks

!!! warning

    Since Jupyter Notebooks contain more than just code, it is hard to keep them in git and collaborate on them. Listen 
    to the linked episode #300 for more details.

## References
- [Episode 300: A Jupyter merge driver for git]({{ urls.python_bytes_300 }})

{% endblock %}
