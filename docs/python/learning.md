---
title: Python learning resources
show_rating: false
tags:
  - Python
  - Learning
---

{% extends 'page_base.md' %}
{% block content %}

## General recommendations

- Start with **single files** (`main.py`) and learn how to run them using the terminal or from the IDE
- Get a **good code editor** and start using it, get familiar with the features (I recommend [PyCharm]({{ urls.pycharm_home }}))
- When you code more often, also start learning how to **use git** and backup your code with [Github]({{ urls.github }}).
- Test your code using `print()` statements
- Learn first the data types, loops, functions and how to create **virtual environments** (`python -m venv venv && source venv/bin/activate`)

!!! tip "Debugging with colors"

    Additionally, you can `pip install rich` and use it to inspect code with `rich.inspect` 
    or replace the builtin print with `from rich import print` to get nicer colors.

## Resources

{% for r in python_resources %}

### [{{ r.title }}]({{ r.url }})
{%- if show_rating %}
Entry: {{ r.beginner * ':star:' }}  
Intermediate: {{ r.intermediate * ':star:' }}  
Advanced: {{ r.advanced * ':star:' }}
{%- endif %}

{% if r.description | default(False) %}{{ r.description }}{% endif %}
{% endfor %}

## Podcasts

{% for r in python_podcasts %}
### [{{ r.title }}]({{ r.url }})

{% if r.description | default(False) %}{{ r.description }}{% endif %}
{% endfor %}

{% endblock %}
