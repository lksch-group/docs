---
title: PyCharm cheatsheet
tags:
  - Python
  - IDE

shortcuts:
  - shortcut: ['alt', 'enter']
    name: 'Toggle menu to **auto-import** python object when selected by cursor'
  - shortcut: ['ctrl', 'f']
    name: '**Search** in current file'
  - shortcut: ['ctrl', 'shift', 'f']
    name: '**Global search**'
  - shortcut: ['ctrl', 'r']
    name: '**Replace** in current file'
  - shortcut: ['ctrl', 'shift', 'r']
    name: '**Global replace**'
  - shortcut: ['ctrl', 'k']
    name: 'Switch to **commit window**'
  - shortcut: ['ctrl', 's']
    name: 'Make PyCharm save the file'
  - shortcut: ['ctrl', 'alt', 's']
    name: 'Open the **settings**'
---

{% extends 'page_base.md' %}
{% block content %}

## Selected keyboard shortcuts

{{ macros.table_header(('Keyboard shortcut', 'Explanation')) }}
{%- for s in shortcuts %} 
{{ macros.table_row((macros.keyboard_shortcut(s.shortcut), s.name)) }}
{%- endfor %}


## Live Templates

Especially for cases like Jinja2, where you use directive like {% raw %}`{% include 'somefile.md' %}`{% endraw %}, it becomes 
handy to use [live templates ]({{ urls.pycharm_live_templates }}). 
It **saves alot of time** and makes **development more fun**.

{% endblock %}
