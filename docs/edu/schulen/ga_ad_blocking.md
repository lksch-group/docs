---
title: Ads und AdBlocking
competences:
 - Wie wird **Werbung im Internet verkauft** und wer sind die Akteure?
 - Mindestens 2 **Konzepte**, wie man **Werbung blockieren** kann
 - Wie funktioniert technisch ein **Browser-Plugin**
 - Technische Unterschiede beim Surfen per Smartphone und über den PC

---
{% extends 'page_base.md' %}
{% block content %}

In dieser Gruppe beschäftigen wir uns mit dem sog. **AdBlocking**. Damit meinen das **Fernhalten von unerwünschter 
Werbung** beim **Surfen im Internet**. {% include 'edu_competences.md' %}

## Werbung im Netz

Wir wollen am Beispiel von **Google** illustrieren, wie Werbung verkauft wird.

### Recherchiere, wie Google Werbung verkauft

- Wie heisst der Google Dienst für Ads?  
- **Logge dich ein** bei diesem Service mit deinem Google Account. 
    - Mache **Screenshots** von interessanten Dingen  
    - Wie kann ich das **Zielpublikum** für Werbung auswählen? 
    - Welche Attribute oder **Selektoren** stellt mir Google zur Verfügung?  

### Werbung auf dem Bildschirm

Versuche eine **Website** mit einer **aufgeschalteten Werbung** zu finden.

- **PC**: Was fällt dir auf, wenn du beobachtest, **wann die Werbung erscheint** im Vergleich zum **Rest der Website**?
    - Mache einen Screenshot und beschreibe deine Beobachtungen unter dem Bild
- **Smartphone**: Finde eine App, die Werbung schaltet innerhalb der App
    - Mache einen Screenshot. Je nach Smartphone gibt es Tastenkombinationen, und dies zu tun, diese findet man auch im Netz.

### AdBlocker auf Smartphone und Computer

Auf **PC** und **Smartphone**, versuche dir die Browser Extension **[uBlockOrigin]({{ urls.ublock_o }})** zu installieren: [Chrome]({{ urls.ublock_o_chrome }}) | [Firefox]({{ urls.ublock_o_firefox }}).

- Schreibt auf, wo es möglich war und wo nicht, und ob man vielleicht eine Erklärung dafür findest
- Nimm nochmals die **Website mit der gefundenen Werbung**, und **aktiviere den AdBlocker**. Gibt es jetzt einen Underschied?

Installiert die Extension **Lightbeam** [Chrome]({{ urls.lightbeam_chrome }}) | [Firefox]({{ urls.lightbeam_firefox }}).

- Surft eure **Lieblings-Websites** an und schaut (**mit und ohne AdBlocker aktiv**), welche **AdSites** auf den Seiten **gemeinsam vorkommen**. 

### Für Nerds: Browser Developer Tools

Findest du heraus, was genau im **HTML-Quelltext** anders ist mit und ohne AdBlocker?

- Öffne mit {{ macros.keyboard_shortcut(['ctrl', 'shift', 'i']) }} die **Browser Developer Tools**. 
- Ohne Adblocker, öffne den **Seitenquellentext** (HTML, "View page source") und kopiere den Inhalt in einen Texteditor. 
- Mache das gleiche mit AdBlocker aktiv, und **vergleiche den Inhalt**. Schaue spezifisch nach `<script>` Elementen. 
- Was wird herausgeschnitten? Findest du URL's in herausgeschnittenen Bereichen?

??? danger "Gefahren von Browser Plugins"
    Bevor man sich ein Browser Plugin installiert, sollte man **unbedingt überprüfen**, ob die Quelle 
    des Plugins **vertrauenswürdig** ist. Im **Zweifelsfall lieber kein Plugin installieren**!

{% endblock %}
