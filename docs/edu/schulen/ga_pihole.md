---
title: PiHole DNS-Blocker
competences:
  - Ablauf Installation eines Betriebssystems (Linux) einmal gesehen haben
  - Aufruf der Kommandozeile, Herausfinden der IP-Addresse
  - Verstehen, was die Aufgabe von DNS im Internet ist
---

{% extends 'page_base.md' %}
{% block content %}

### PiHole: DNS-AdBlocking

Mit der Demonstration der PiHole Software auf einem SBC holen wir uns einen Teil des Internets ins Zimmer, 
oder besser gesagt: Wir betreiben unser eigenes kleines Telefonbuch, um Namen in IP-Addressen aufzulösen. {% include 'edu_competences.md' %}

### Vorbereitung

Vorab ein paar Recherche-Fragen:

- Was ist eine LAN-IP?
- Finde heraus, welche IP dein Computer gerade hat. Versuche es mit einem Terminal-Command. Schreibe sie dir auf.
- Versuche anschaulich zu erklären, was die Aufgabe von DNS-Server im Internet ist
- Was bedeutet `8.8.8.8` und `9.9.9.9`?

### Kurzvorstellung Raspberry Pi

- Komponenten des Computers herausfinden
- Merkmale der Komponenten erkennen

### Installation OS

- Image **Raspberry Pi OS (64bit)** schreiben mit [Raspberry Pi Imager]({{ urls.raspi_software }})
- Raspi starten (mit Bildschirm/Beamer falls möglich)
- Pihole installieren mit `curl -sSL https://install.pi-hole.net | bash` gemäss [Anleitung]({{ urls.pihole_onestep }})

### Was Smartphone Apps im Hintergrund treiben...

Jetzt das wir ein PiHole haben, können wir beobachten, welche Websites Apps im Hintergrund zu erreichen versuchen.

1. Verbinde ein Smartphone mit dem Router, wo auch das PiHole angeschlossen ist.
2. Suche in den [Einstellungen beim Netzwerk](https://www.lifewire.com/how-to-change-dns-on-android-4588645), ob du bei der Verbindung einen eigenen (custom) DNS-Server eintragen kannst
3. Wir beobachten mit dem Befehl `sudo pihole -t` auf dem Raspi, was jetzt passiert

{% endblock %}
