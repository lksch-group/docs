---
title: Fediverse
tags:
  - Social Media
hide:
  - navigation
---
{% extends 'page_base.md' %}
{% block content %}

<figure>
  <img src="../img/Fediverse_AllProcotols_v1.1_13.05.2022.png" width="750" align="center" />
  <figcaption>Illustration Fediverse von Mike Kuketz</figcaption>
</figure>

Das **Fediverse** ist eine Wortkombination as **fed**erated un**iverse**. Es bezeichnet konkret das [föderierte Univserum 
von Social-Media-Diensten]({{ urls.fediobserver_map }}). Wir verdanken dem Herrn Elon Musk, dass sich gerade viele Personen mit den 
Alternativen zu Twitter beschäftigen. Das sieht man auch gut an den Nutzerzahlen im Fediverse bei der Platform Mastodon.

## Fedigov

Es ist jedem selber überlassen, ob er solche Dienste braucht oder nutzen möchte. Wenn staatliche Stellen oder auch staatliche 
Medien informieren, sollten sie dies auch auf den föderierten Platform tun, damit diese nicht aussen vor bleiben.

{% set item = fedigov_caa %}
{% include 'fw_button.md' %}

## Weitere Quellen

{% for link in fediverse_links %}
- [{{ link.title }}]({{ link.url }})
{% endfor %}

{% endblock %}
