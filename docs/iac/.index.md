---
title: Infrastructure as Code
ansible_pros:
  - easy to start
  - very flexible and extensible
  - easy to debug things or run selected tasks
  - has modules that plug to Cloud Provider API's

ansible_cons:
  - no clear way to organize things
  - relatively slow for large quantities of jobs
  - You can not use functions or such things
  - cloud support with modules can be incomplete (e.g. Google Cloud)

puppet_pros:
  - Changes are applied fast and efficiently
  - good for large fleets of servers
  - Can be used on localhost
puppet_cons:
  - hard to test and setup locally
  - more development effort and harder debugging

terraform_pros:
  - allows cloud-agnostic IaC (AWS, Google)
  - very widely supported
terraform_cons:
  - has it's own language to learn (HCL)
  - moderately steep learning curve
---

{% extends 'page_base.md' %}
{% block content %}

# Infrastructure as code

There are many infrastructure as code tools out there, the most known are probably **ansible**, **puppet**, **chef** and 
**terraform**. 

## Ansible

Ansible I would recommend to get started when first adapting the IaC philosophy. It has a relatively moderate learning curve.

- Uses **ssh** to connect and apply changes to servers
- Can be seen as **task runner** with builtin idempotency
- Es extensible using collections, has a package installer named **ansible-galaxy**
- Tasks run in sequence in a declarative way
- based on **python** programming language, uses **Jinja2**

{{ macros.pros_cons(ansible_pros, ansible_cons, tabbed=true) }}

## Puppet

Puppet needs a **puppet server** and **puppet clients** must register with the server. They contact the server 
in defined intervals using the **puppet agent** installed on the client.

- Uses **server/client model**
- Applies changes **based on a catalogue** that is compiled using **desired and actual state** of clients
- based on **Ruby** programming language

{{ macros.pros_cons(puppet_pros, puppet_cons, tabbed=true) }}

## Terraform

Terraform by HashiCorp is an **infrastructure provisioning** tool and not targeted to configure servers. Thus it is often used 
with e.g. ansible together. It abstracts away objects like networks, vm's etc. so that it can be applied to 
different cloud providers. Changes of the infrastucture can be tracked and diffs can be checked before being applied 
using a **state**.

{{ macros.pros_cons(terraform_pros, terraform_cons, tabbed=true) }}

{% endblock %}
