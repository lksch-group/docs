---
title: Bash Cheatsheet
tags:
  - bash
  - shell
---

{% extends 'page_base.md' %}
{% block content %}

# bash cheatsheet

## History

### Last run commands

Traverse the history in `~/.bash_history`

{{ macros.keyboard_shortcut(['arrow-up']) }}  

or if you forget `sudo`, reference the last command using `!!`:

```shell
sudo !!
```

### Search last commands

{{ macros.keyboard_shortcut(['ctrl', 'r']) }} will toggle inverse search in your history.

### Arguments of last successfull command

We ran:

```shell
cat log-edabadfdads-xasdsf.text 
```
but the log is too big. So we want to tail it. You can write `tail ` and hit {{ macros.keyboard_shortcut(['alt', 'period']) }} to get the last argument.

## Signals

## Exit a program

{{ macros.keyboard_shortcut(['ctrl', 'c']) }} will normally exit any shell program.

## Exit Codes

### Exit code of last run command

`echo $?` will show the exit code of the last run command.

## Clipboard

### Paste from clipboard

{{ macros.keyboard_shortcut(['ctrl', 'shift', 'v']) }} pastes clipboard content to the terminal.

## Shell tricks

- {{ macros.keyboard_shortcut(['ctrl', 'k']) }} cuts characters **right to the cursor**
- {{ macros.keyboard_shortcut(['ctrl', 'u']) }} cuts characters **left to the cursor**
- {{ macros.keyboard_shortcut(['ctrl', 'y']) }} **bring back last cut characters**

### Open buffer

When starting a line like a loop:

```shell
for item in "1 2 3 4"; do
```

you can hit {{ macros.keyboard_shortcut(['ctrl', 'x', 'e']) }} to open a buffer in your editor (`echo $EDITOR`). Complete the code with 
```shell
for item in "1 2 3 4"; do
   echo $item
done
```
Quit the editor with saving and the content will run.

## Navigating directories

### Go to the home directory

Change to the home directory of `$USER`.

```shell
cd ~
cd $HOME
```

### Go back to last directory

`cd -` will go back to the last directory

## Inspecting files

### End of file

Display the end of a logfile following new incoming data:

```shell
less +F /var/log/syslog
```

- Hit {{ macros.keyboard_shortcut(['ctrl', 'c']) }} to **detach from the end** of the file to scroll up.
- Hit {{ macros.keyboard_shortcut(['shift', 'f']) }} to **glue to the end**.

Display last 100 lines of a file with `tail`:

```shell
tail -n 100 /var/log/syslog
```

## Savers

### Reset the terminal when behaving wierd

`reset` wil reset the terminal, which can be used **instead of closing the terminal**.

## References

- [Advanced Bash Scripting]({{ urls.advanced_bash_scripting }})
- [Shell Ninja: Mastering the Art of Shell Scripting | Roland Huß]({{ urls.talk_shell_ninja }})

{% endblock %}
