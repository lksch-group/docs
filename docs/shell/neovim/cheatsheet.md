---
title: neovim cheatsheet
tags:
  - neovim
  - cheatsheet
  - shell
sections:
  Basics: basics
  Navigation: navigation
  Modifications: modifications
  "Visual Mode": visual_mode
  "Vim commands": vim_commands
  "Custom Indentation": custom_indentation

url_refs:
  "Vim Crash Course For Beginners": vim_crash_course
  "Effective Neovim: Instant IDE": effective_neovim

---
{% extends 'page_base.md' %}
{% block content %}

{%- macro codify(text) -%}
`{{ text }}`
{%- endmacro -%}

# neovim cheatsheet

{% for section, key in sections.items() %}

## {{ section }}

{% set data = vim_cheatsheet[key] %}
{{ macros.table_header(("Keys", "Explanation")) }}
{%- for cmd, info  in data.items() %}
{{ macros.table_row((codify(cmd), info)) }}
{%- endfor %}
{% endfor %}

## References

{% for label, key in url_refs.items() %}
- [{{ label }}]({{ urls[key] }})
{% endfor %}

{% endblock %}
