---
title: screen cheatsheet
server: host.mydomain.tld
---

# screen cheatsheet

`screen` is a tool to open shell sessions on sockets, so that you can 
disconnect from a shell session on a remote server connected through ssh without loosing 
running commands in them.

## Open a socket under a name

On the remote server connected using `ssh {{ server }}`, run

```shell
screen -S monitor htop
```
Close the shell. Now you can connect to the session using ssh directly, re-attaching to the session:

```shell
ssh -t {{ server }} screen -r monitor
```
