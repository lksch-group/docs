---
title: Linux FAQ
tags:
  - Linux
  - FAQ
---
## Warum sollte ich mich mir ein Linux installieren/ausprobieren?

- Weil es heute eines der **besten Betriebssysteme** ist, die es gibt, auch für Anfänger
- Ein Linux ist **kein kleiner Spion** wie Windows
- Eine riesengrosse **Community** and Entwickler und Hobbyisten daran arbeiten, mit unglaublichem Tempo
- Du musst fast nie unsicheren Programme aus dem Internet herunterladen und Viren bekämpfen
- Keine Neustarts nach Updates
- Keine Systemcrashes, keine fehlerhaften Updates
- Sehr guter Hardware Support für Computer, ideal auch für **ältere Geräte** (siehe die Systemanforderungen von Windows 11)
- **~90% der Games** auf Steam kann man mit Linux spielen (mit Proton/Wine), was dir kein Mac bietet!
- Linux ist auf **mehr als 90% der Computer auf der Welt** installiert, du bist in guter Gesellschaft!
- Mac und Linux haben die gleichen Vorfahren: Dateisystem und Eingaben auf der Kommandozeile sind fast identisch. Du schlägst zwei Fliegen mit einer Klappe
- Hast du ein mal Problem, findest du **sofort Antworten im Internet** während es bei Fehlern in Microsoft eher schwierig wird.
- Sofern du nicht von spezifischen Programmen wie Adobe abhängig bist, gibt es sehr gute Chancen, dass es für alle deine Bedürfnisse ein passendes Programm gibt
- Viele Windows-Programme können auch auf Linux betrieben werden (siehe die App [bottles](https://usebottles.com/))
- Das Ökosystem an toller, freier Software ist auf Linux viel grösser, und viele der Programme gibts auch für Windows, was eine Zusammenarbeit möglich macht
- Solltest du später oder heute schon programmieren, wird Windows meist zum Hindernis und du verlierst Zeit
- Der Markt für OpenSource wächst jedes jahr um mehr als 10%. Auch der Staat hat sich mit "Public Money, Public Code" zum OpenSource Gedanken bekannt.
- Junge und innovative [Firmen setzten immer mehr auf OpenSource](https://heise.de/-6536531)

Die Welt macht hier gerade einen Umbruch: Microsoft ist auf einmal voll auf dem OpenSource Trip, Dell verkauft Laptops mit Ubuntu Linux, Brother gibt Treiber für Linux heraus, es gibt für Streamer Videocards mit Linux Treibern von Blackmagic, Valve (Steam) meint es Ernst, dass sie Linux fürs Gaming immer besser unterstützen wollen und investieren Millionen (auch nachdem Google Stadia gescheitert ist).

Weiter zu **[Linux v.s. Windows](../../linux_vs_win/)**

## Kann ich Linux neben Windows betreiben?

- Als DualBoot neben Windows installiert, kann man wahlweise in Windows oder Linux starten. Linux kann das NTFS-Dateisystem von Windows zumindest lesen. Beim Dateienaustausch wird es schwierig.
- Linux in einer Virtuellen Maschine auf Windows (eher kleiner Privatsphärengewinn)
- Windows als Virtuellen Computer von Linux aus Starten für einzelne Programme. Man benötigt ein Windows-Installationsmedium.

## Welche Vorteile bietet Linux gegenüber Windows?

- **Privatsphähre**: Wenn man ehrlich ist, dann ist Windows ein Spionagegerät:
    - Tastatureingaben werden für die verbesserte Nutzererfahrung ausgewertet
    - Telemetriedaten lassen sich nicht ausschalten
    - Es wird dem Nutzer zunehemnds schwerer gemacht, das System ohne Account bei Microsoft zu betreiben
    - Bei Windows 11 Home wird es nicht mehr möglich sein, dieses ohne Microsoft-Account zu betreiben ([siehe Changelog Microsoft](https://blogs.windows.com/windows-insider/2022/02/16/announcing-windows-11-insider-preview-build-22557/))
    - Microsoft wird über kurz oder lang auch das Office nur noch als Cloud-Lösung anbieten, womit das Arbeiten offline erschwert wenn nicht verunmöglicht wird
    - **MacOS** kann man am besten als goldenen Käfig beschreiben. Wenn Apple deine Daten gut gegen Dritte schützt, heisst das nicht, dass wenig Daten bei Apple anfallen

- **Keine Abhängigkeit**:
    - Es kann sein, dass in Zukunft die **Windows-Lizenz mit dem Gerät verkoppelt** wird. Es wäre dir nicht möglich, ein Backup deines Windows auf einen neuen Computer zu laden. Dann gibt es zahlreiche Lizenzen, der dir das eine erlauben, das andere nicht.
    - Windows alleine kann Fehler beheben, und du wirst wenig bis keinen Support finden, wenn du wirklich einmal ein Problem hast. Das ist auch bei einer Reparatur-Firma der Fall würde sie sich an Microsoft wenden wollen.

- **Bessere Features**: Der Gnome Desktop integriert Kalender, Kontakte, Apps und Notifications und die globale Suche in einem System. Features wie **DarkMode** und **Nightlight** für ermüdete Augen findet man vergeblich bei Windows.

- **Anpassbarkeit**: Verschiedene Desktop-Umgebungen sind verfügbar (meistgenutzte zuerst), wobei Distros eine oder mehrere dieser unterstützen: 
    - [Gnome](https://www.gnome.org/) mit [Gnome Shell Extensions](https://extensions.gnome.org/)
    - [KDE](https://kde.org/)
    - [Cinnamon](https://de.wikipedia.org/wiki/Cinnamon_(Desktop-Umgebung)) (Linux Mint)

- **Zeitersparnis**:
    - Bei einem gekauften Gerät muss ich all diese vom Hersteller vorinstallierten Dinge entfernen (sog. Bloatware).
    - Möchte ich dann ein frisches Windows installieren, habe ich damit viel viel länger als mit einer Linux Distro.
    - Installation von Programmen über einen App Store geht schneller als jedes Programm einzeln aus dem Internet von Hand herunterzuladen. Das ist für viele Windows Programme immer noch der Fall und daneben unsicher.
    - Ich muss auf keine Neustarts warten. Systemcrashes sind seltener als bei Windows und auch bei Updates hat Microsoft (denke wegen der schieren Komplexität ihrer Produkte und Lizenzen) mehr Fehler drin.
    - Ich habe die Erfahrung gemacht, dass Drucker nach wenigen Sekunden nach Anschliessen per USB drucken konnten: mit Linux Mint!

- **Komfort**:
    - Updates von Apps über einen Klick, man wird benachrichtigt, wenn es Updates gibt, auch da keine Ausflüge ins Internet mehr nötig
    - Mit Apps wie [**KDEConnect**](https://kdeconnect.kde.org/) oder [**GSConnect**](https://extensions.gnome.org/extension/1319/gsconnect/) kannst du vom Smartphone (oder einem anderen Linux) Dateien austauschen oder deinen PC steuern
    - Drucker werden sehr gut erkannt und man kann meist nach wenigen Sekunden drucken, ohne irgendwelche Treiber installieren zu müssen ([driverless printing](https://wiki.debian.org/CUPSDriverlessPrinting)).

- **Exzellente Hardware-Unterstützung**
    - Alte Geräte können weiter betrieben werden, damit du länger Freude daran hast.
    - Es gibt speziell Linux Distros, die auch auf >10 Jahre alten Geräten noch flüssig laufen, z.B. [Ubuntu Mate]({{ urls.ubuntu_mate }})

{% include 'glossary.md' %}