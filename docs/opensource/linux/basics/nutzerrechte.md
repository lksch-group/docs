---
---
{% extends 'page_base.md' %}

{% block content %}
Der erste grundsätzliche Unterschied zwischen Linux/MacOS und Windows sind die **Berechtigungen**, die ein Nutzer standardmässig auf dem System besitzt. Bei Windows gibt es den **Admin** und den **PowerUser**, der keine Admin-Rechte hat und nur sehr beschränkt selber Programme installieren kann. Daher wird ein **Windows-PC meist mit einem Nutzer mit Admin Rolle eingerichtet**.
Bei Linux und MacOS hingegen gibt es als Äquivalent zu den Admins die **sudoers**, die Gruppe der Admins. Der erste Account wird i.d.R. Teil dieser Gruppe, muss seine Admin-Rechte aber bei jeder Aktion, die solche verlangt, mit seinem Passwort bestätigen. Der Benutzer ist standardmässig unpriviliegiert, und kann Admin werden mit `sudo`, wobei das Passwort abgefragt wird.
{% endblock %}
