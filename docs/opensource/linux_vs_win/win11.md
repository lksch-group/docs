---
title: Win11
tags:
 - Windows
 - Linux
---
# Windows 11

Nach 7 Jahren seit dem Release von Windows 10 wird Windows 11 ab Oktober 2021 erwartet. Hier die wichtigsten Infos zusammengetragen.

- Prozessoren von [Intel](https://docs.microsoft.com/en-us/windows-hardware/design/minimum/supported/windows-11-supported-intel-processors) um/älter 2017 sowie [AMD](https://docs.microsoft.com/en-us/windows-hardware/design/minimum/supported/windows-11-supported-amd-processors) Prozessoren um/älter 2018 werden schon nicht mehr unterstützt :biohazard:
- **Viele PC's gekauft 2018 und 2019 sind damit nicht mehr kompatibel**!!!
- Für Windows Home **muss ein Microsoft Account** verwendet werden, d.h. es ist fraglich, wie nützlich der Computer ohne Internet-Verbindung noch ist
- SecureBoot muss unterstützt sein (Aktivierung ist optional, Dual-Boot-Systeme sind möglich)
- TPM muss aktiviert sein
- [Windows 10 Update-Support noch bis 2025](https://www.heise.de/newsticker/meldung/Microsoft-nennt-Datum-fuer-Support-Ende-von-Windows-10-2795401.html)
- Feature-Updates nur noch jährlich (Upgrades)

Hier der Auszug zu den [Neuerungen vom  Windows 11 Insider Preview Build 22557](https://blogs.windows.com/windows-insider/2022/02/16/announcing-windows-11-insider-preview-build-22557/):

> Similar to Windows 11 Home edition, Windows 11 Pro edition now requires internet connectivity during the initial device setup (OOBE) only. 
> If you choose to setup device for personal use, MSA will be required for setup as well. 
> You can expect Microsoft Account to be required in subsequent WIP flights.

## Links

- [Don't buy a new PC for Windows 11](https://yewtu.be/watch?v=NivpAiuh-s0)
- [Windows 11: The Good, the Bad & the Ugly](https://yewtu.be/watch?v=CH0bAeEm1Jw)
- [Should Gamers Stick to Window 10?](https://yewtu.be/watch?v=21jH39rlvDA)
- [Install Linux instead of Windows 11](https://yewtu.be/watch?v=_Ua-d9OeUOg)

{% include 'glossary.md' %}
