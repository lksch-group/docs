---
tags:
  - Linux Einstieg
  - Linux Intro
---
{% extends 'page_base.md' %}
{% block content %}

# Wie sich ein Umstieg gestaltet

Sich auf ein komplett neues OS einzustellen, ist sicher am Anfang nicht ganz einfach. Die meisten Personen kennen Windows bereits, 
einige haben Mac und Windows benutzt, wo es auch sehr viele Unterschiede gibt (z.B. die Tastatur). Das heisst also, **würdest du dir 
den Umstieg von Windows auf Mac (oder umgekehrt) zutrauen, dann wirst du auch diese Aufgabe meistern**. 
Die OS, die ich hier für für Neueinsteiger/innen empfehle, sind aber sehr konfortabel und intuitiv zu bedienen, so easy wie ein Mac. Die wichtigsten Punkte sind:

- Nebst dem Betriebssystem sind es **allem voran die Programme**, die man benutzt. Sofern du auf Windows / Mac bereits darauf geachtet hast, dass du vornehmlich freie Software einsetzt, wird es diese auch auf Linux geben
- Man soll nicht mit der Erwartungshaltung kommen, dass Linux genau wie Windows ist und dann enttäuscht sein.
- Eine Einführung durch jemanden, der sich gut auskennt und dir am Anfang den Weg weist und die Konzepte erklärt, hilft, schnell den *sicheren Hafen* zu ereichen.
- Mit einer **guten Einführung** wirst du dich ziemlich sicher sehr schnell sehr wohl fühlen, und dies für den Rest deines Lebens!

## Konzepte

### Wie eine Distro zusammengesetzt ist

> Eine [Linux-Distribution](https://de.wikipedia.org/wiki/Linux-Distribution) ist eine Auswahl aufeinander abgestimmter Software um den Linux-Kernel, 
> bei dem es sich dabei in einigen Fällen auch um einen mehr oder minder angepassten und meist in enger Abstimmung mit Upstream selbst gepflegten Distributionskernel handelt.

### Installation von Software ganz anders

Diese *abgestimmte Software* ist über die **Repositories** erreichbar. Weitere Software-Quellen können eingebaut werden, wie z.B. *Flatpak* oder *AppImages* oder *Snaps*. 
Der AppStore integriert diese Quellen, sodass du dort einfach Software installieren und deinstallieren kannst. 
Deine Software wird danach zuverlässig up-to-date gehalten und in den Paketquellen wird jeweils sichergestellt, 
dass alle deine Programme und deren Abhängigkeiten kompatibel zueinander sind und bleiben. Genau wie auf dem Mobile.

### Jeder kann machen was er will v.s. einer bestimmt über alles

Du kannst dir vorstellen, dass am Ende was ganz anderes herauskommt, wenn **Millionen von Personen frei an etwas arbeiten** in einer riesigen verzweigten Baumstruktur. 
Du kannst dir den sehr komplexen [Baum der Distributionen](https://distrowatch.com/dwres.php?resource=family-tree) anschauen. 
Daher ist es schwierig, sich am Anfang für irgend etwas zu entscheiden, da es einfach viel zu viel Auswahl und Kombinationen gibt. 
Die OpenSource-Natur der Software und verschiedene Meinungen führen zu Aufsplittungen von Projekten und einer grossen Zahl an Möglichkeiten. 
Es ist dann schwierig, sich im Dschungel zurechtzufinden, man sollte aber **diese Vielheit als etwas Positives** sehen. 
Gerade auch weil ich wir Menschen und unsere Anforderungen genauso vielfältig sind.

Die Entdeckung dieser Möglichkeiten braucht Zeit. Das Konzept von Linux lautet: Du kannst alles anpassen was du willst. 
Das heisst, du musst zuerst einmal herausfinden, was alles möglich ist, um deine Lieblings-Features zu finden. Der Weg ist das Ziel!

### Welche Distro soll ich für mich wählen?

Idealerweise hat man jemanden, der einem beim Umstieg an die Hand nimmt und dich durchs erste Dickicht führt bis zur ersten Lichtung, wo du dich dann einrichten kannst. Das beinhaltet: die Wahl der ersten Distro, Tipps für Programme als Alternativen, die man kennt, die wichtigsten System-Features.

Eine **Distro kann man mit einer Live-CD/ einem Live-USB ausprobieren** um zu testen, ob der Computer gut mit dem System ohne Anpassungen zusammenspielt. **Das Alter des Computers** ist relevant, deine Vorlieben für die **Desktop-Umgebung** sowie dein **Anwendungszweck**. So unterschiedlich Personen und Vorlieben sind, so viele Möglichkeiten gibt es auch bei den Distros.

Weitere Infos zur Distro-Wahl [hier](/opensource/linux/basics/distro_choice).

### Betriebssystem zum mitnehmen

Wie bereits vorher erwähnt, lassen sich die meisten Linux Distros zuerst im Live-Mode ausprobieren. Wäre es nicht praktisch, 
jeden Computer von einem USB-Stick starten zu können mit deinen Einstellungen, wie du es gewohnt bist?

- [Lernstick]({{ urls.lernstick_about }}) der **Universität Bern** ist ideal für Schüler.
- [Tails OS]({{ urls.tails_os }}) ist ein weiteres Beispiel, welches mit Fokus auf Privacy und Security entwickelt wurde

## Referenzen

- [Distrotube - Windows Users Need To Know This Before Switching To Linux](https://yewtu.be/watch?v=fvkc5WzciHw)

{% endblock %}