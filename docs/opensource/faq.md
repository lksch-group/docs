---
title: OpenSource FAQ
tags:
  - Bildung
  - Digitaliserung
  - Fachkräftemangel
---

{% extends 'page_base.md' %}
{% block content %}

## Wie erkenne ich ein gutes OpenSource Projekt?

Indikatoren für die "Gesundheit" eines Projekts lassen sich auf dem Repository finden, welches bspw. auf Github liegt. Diese sind:

- Wann wurden das letzte Mal Änderungen gemacht? Wird das Projekt noch aktiv gepflegt?
- Wie viele offene Issues gibt es?
- Wie viele Stars hat das Projekt auf Github?

Die Bekanntheit bestimmt meist auch über die Grösse des Ökosystems rund um die Software:

- Ist es sehr bekannt, dann finden sich auch auf viele Fragen und Probleme, die Nutzer einmal hatten, Antworten im Netz, was das lernen vereinfacht
- Danaben existieren mehr Erweiterungen und auch andere Software, die auf dieser aufbaut

## Warum sollte ich programmieren lernen?

Wir alle Lernen in der Schule lesen und schreiben, und die wenigsten von uns werden Schriftsteller. 
Ein Programm-Code ist nichts weiter als ein Stück Text, welches die Abfolge von Schritten beschreibt, 
die ein Computer ausführen soll. Da wir heute fast alle in irgendeiner Form mit Computern arbeiten, 
warum sollte man also nicht programmieren lernen?

## Wo kann programmieren lernen?

Es gibt hierzu sogar kostenlose Angebote:

**Zürich**: 

- [Git und Github](http://opentechschool.github.io/social-coding/)
- [Open Tech School](https://opentechschool-zurich.github.io/)
- [Kurse Progammieren](https://openki.net/find?categories=programming)


**Im Netz:**

- :fontawesome-brands-youtube: [JavaScript InDepth](https://www.youtube.com/watch?v=Bv_5Zv5c-Ts)  
- :fontawesome-brands-youtube: [pythonprogramming.net](https://pythonprogramming.net/python-fundamental-tutorials/)  
- :fontawesome-brands-youtube: [Python Corey Schafer](https://www.youtube.com/user/schafer5)

**Inspirationen?**

- :fontawesome-brands-youtube: [The Art of Code - Dylan Beattie](https://yewtu.be/watch?v=6avJHaC3C2U)

{% endblock %}