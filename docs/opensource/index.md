---
title: Was ist Opensource?
tags:
  - Opensource
  - FAQ
---

{% extends 'page_base.md' %}
{% block content %}

Die Väter des OpenSource Gedanken ([Richard Stallmann](https://de.wikipedia.org/wiki/Richard_Stallman) z.B.) haben sich in den 90er Jahren ausgedacht, 
wie freie Software entwickelt werden sollte. Hierzu wurde die **GNU General Public Licence** ([GPL](https://de.wikipedia.org/wiki/GNU_General_Public_License)) geschaffen. Diese Lizenz regelt die Nutzer der Software und besagt, dass die Software 

- beliebig verändert, 
- nach belieben geschäftlich und privat genutzt,
- und weiterverteilt werden darf. 

Darin ist auch Konzept des [**CopyLeft**](https://de.wikipedia.org/wiki/Copyleft) enthalten:

> Das Copyleft ist eine Klausel in urheberrechtlichen Nutzungslizenzen, die den Lizenznehmer verpflichtet, jegliche Bearbeitung des Werks 
> (z. B. Erweiterung, Veränderung) unter die Lizenz des ursprünglichen Werks zu stellen. 
> Die Copyleft-Klausel soll verhindern, dass veränderte Fassungen des Werks mit Nutzungseinschränkungen weitergegeben werden, die das Original nicht hat. 

Sharing is Caring!

## Wo ist OpenSource Software im Einsatz?

Meist ist sog. OpenSource Software nicht direkt sichtbar, kommt aber überall zum Einsatz. Hier ein paar Beispiele:

- **90% der Geräte im Internet**, mit denen du resp. deine Geräte kommunizieren (Server), werden mit einem Linux-basierten Betriebssystem betrieben: Mailservices, Webseiten etc.
- Die Mehrheit der **Smartphones und Tablets** läuft unter Android, was im Kern ein offenes Betriebssystem basierend auf Linux ist.
- Die Software auf deinem **Router** oder **Fernseher** zu Hause ist ziemlich sicher ein Linux-System.
- Animationen von **Lord of the Rings** wurden alle mit Linux gemacht, bei den Disney und anderen Filmstudios wird hauptsächlich Linux eingesetzt (Quelle: [Destination Linux Folge 266](https://destinationlinux.org/epissode-266/))
- Zahlreiche Komponenten und Standards, die für den Betrieb des Internets notwendig sind wie Verschlüsselung sind ebenfalls OpenSource.

Die Vorteile von OpenSource im Vergleich zu Closed Source hat [Chris Titus in einem Video]({{ urls.ctt_why_opensource }}) gut zusammengefasst.

{% for item in follow_up %}
{% include 'fw_button.md' %}
{% endfor %}

{% endblock %}