---
title: Linux auf Mac-Hardware
tags:
  - Linux
  - MacOS
  - OSX
  - Apple
---

{% extends 'page_base.md' %}
{% block content %}

Es ist wie auch bei anderen Computer-Herstellern möglich, ein gebrauchtes Apple Gerät 
mit Linux wieder zum Leben zu erwecken.

## Welches Linux für den Mac?

Je nach Alter des Geräts muss man eventuell auf ein älteres OS zurückgreifen.

- Ab **>= 4GB RAM** und Jahrgang um 2015 und neuer kann man gut ein aktuelles OS wie z.B. [Linux Mint]({{ urls.linux_mint }}) installieren
- Auf älteren Geräten kann man gut [Ubuntu Mate]({{ urls.ubuntu_mate }}) benutzen

## Wie Linux installieren?

Das Vorgehen ist recht identisch mit einem Windows. Dabei ist es wie gesagt immmer praktisch, dass man das Linux booten 
und ausprobieren kann, bevor man etwas auf die Festplatte schreibt (**Live Modus**).

1. Ein Linux `*.iso` Image herunterladen und auf einen USB-Stick schreiben
2. Nach dem Start, halte {{ macros.keyboard_shortcut(['alt']) }} gedrückt um von einem anderen Datenträger zu booten
3. Boote vom USB Stick
4. Im Live-Modus, teste ob WIFI, das Trackpad, die Hotkeys (Volume, Tastaturbeleuchtung, etc.) funktionieren.

### Sicherheit

- [Apple stopft aktuell bekannte Sicherheitslücken nur im aktuellsten OS]({{ urls.osx_sec_bug }})

## Kurz-Anekdoten

### Mac OS und Linux Brüder?

Bei meinem ersten IT-Job durfte ich am Anfang entscheiden, ob ich ein Apple Laptop (wie alle anderen Entwickler) haben möchte, oder 
aber ein Gerät meiner Wahl mit einem Linux. Ich hab mich **gegen den Mac entschieden**, obwohl unter 
Entwicklern Macs mehr verbreitet sind (an Konferenzen sieht man gut 80% der Teilnehmer mit Mac rumlaufen). Der Grund: Mit dem Mac wusste ich, 
ich bin ans Mac OS gebunden und Apple verlangt **für ein paar GB mehr RAM den 4x Preis** und die Geräte sind **kaum upgradebar/reparierbar**.  

Das onboarding dauerte 2 Tage und von der Vielzahl der bash scripts, die es in der Firma gab, musste ein einziges auf Linux angepasst werden, weil es zwischen 
Mac OSX und bash auf Linux bei einem `tar`-Befehl leicht andere Kommandozeilen-Parameter gab. **Ansonsten war alles zueinander kompatibel.**  
**Fazit**: Als Entwickler spielt es am Ende nicht so eine Rolle welches OS man nimmt, beide sind exzellente (Entwickler-)OS.

### Linux Mint auf Macbook Air 2015

Neulich dachte ein Mitglied der Familie, ein älteres **Macbook Air** würde nicht mehr funktionieren. Dem war nicht so. Ich hab darauf Linux Mint installiert. 
Und [RetroPi]({{ urls.retropie }}) ebenso. Ich habe das Macbook Air dann meiner Mutter auf den Geburtstag geschenkt, damit sie wieder einen Computer hat und 
darauf gleichzeitig alte Mario-Klassiker spielen kann. **Sudoku, Solitaire und Mahgong** sowie weitere Spiele findet man dan meist **im App Store der Linux Distro**. 

{% endblock %}
