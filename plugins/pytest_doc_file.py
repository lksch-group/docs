import builtins
import inspect
import re
from collections import defaultdict
from pathlib import Path
from textwrap import indent, dedent

import pytest
from rich import print as rprint, inspect as rinspect

indent_rgx = re.compile(r"^\s*")
test_func_rgx = re.compile(r"def test_[a-z_0-9]+\(\):")
assert_rgx = re.compile(r"^assert")
four_lines_rgx = re.compile(r"\n{4,}")


def double_q(text):
    return f'"""\n{text.strip() if isinstance(text, str) else text}\n"""\n'


def get_indent(line: str, default=""):
    m = indent_rgx.match(line)
    return m.group(0) if m else default


def test_unwrap(file_content: str):
    splits = test_func_rgx.split(file_content)

    assert len(splits) > 1, "No test functions inside file"

    def handle_block(bl: str):
        yield dedent(bl)

    for block in splits:
        yield from handle_block(block)


to_append = defaultdict(dict)


def print_append(*args, **kwargs):
    frame = inspect.currentframe().f_back
    file_path = Path(frame.f_code.co_filename)
    line_number = frame.f_lineno
    to_append[file_path][line_number] = args[0]
    rprint(*args)


def patched_lines(p: Path, injects: dict[int, str]):
    for ix, line in enumerate(p.read_text().splitlines()):
        yield line
        include = injects.get(ix + 1)
        if include is None:
            continue
        indent_str = get_indent(line)
        yield indent(double_q(include), prefix=indent_str)


def handle_test_file(
    file_path: str, injects: dict[int, str], write: bool = False, fn_prefix: str = ""
):
    p = Path(file_path)
    lines = patched_lines(p, injects)
    lines = tuple(test_unwrap("\n".join(lines)))
    lines_out = four_lines_rgx.sub("\n" * 3, "\n".join(lines))
    if write:
        return (p.parent / f"{fn_prefix}{p.parts[-1]}").write_text(lines_out)
    return rprint(lines_out)


# def pytest_add_option_write(parser):
#     parser.addoption(
#         "--doc-write", action="store_true", help="Write converted tests to files"
#     )
#

@pytest.fixture(scope="session", autouse=True)
def patch_print(request):
    fn_prefix: str = "doc_"
    orig_print = builtins.print
    builtins.print = print_append
    # write = request.config.getoption("--doc-write")
    yield
    builtins.print = orig_print

    for fp, injects in to_append.items():
        handle_test_file(fp, injects, fn_prefix=fn_prefix, write=True)
