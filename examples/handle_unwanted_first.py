from datetime import datetime


# this is not very readable, return None is implicit here if start or end is not given
def time_between(start: datetime = None, end: datetime = None):
    if start:
        if end:
            return end - start


# this is more readable und we don't need to indent as much
def time_between_better(start: datetime = None, end: datetime = None):
    if not start or not end:
        # same as return None
        return
    return end - start
