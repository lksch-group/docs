#!/usr/bin/env bash

version=$(grep -Po '^version\s=\s"\K\d\.\d\.\d' pyproject.toml)
target=${1:-development}
app_name=$(grep -Po '^name\s=\s"\K[\w_-]+' pyproject.toml)
python_version=3.10.7
dev_uid=$(id -u)

function build {
    docker build --tag $app_name:$target --target $target \
      --build-arg PYTHON_VERSION=$python_version --build-arg DEV_UID=$dev_uid .
    echo "$app_name:$target"
}

function build_prod {
    docker build --tag $app_name:$version --target $target \
      --build-arg PYTHON_VERSION=$python_version --build-arg DEV_UID=$dev_uid .
    echo "$app_name:$version"
}

function cleanup() {
  docker image prune -f && docker container prune -f
}

if [ "$target" = 'production' ]; then
  build_prod
else
  build
fi

cleanup > /dev/null