from pathlib import Path

from confz import ConfZ, ConfZFileSource
from pydantic import SecretStr, AnyUrl

FILE_PATH = Path(__file__)
ENV_FILE = FILE_PATH.parent / "confz_env.yml"


class DBConfig(ConfZ):
    user: str
    password: SecretStr


class APIConfig(ConfZ):
    host: AnyUrl
    port: int
    db: DBConfig

    CONFIG_SOURCES = ConfZFileSource(file=ENV_FILE)


cfg = APIConfig()
assert cfg.port == 1111
assert cfg.db.user == "admin"
print(cfg)
"""
host=AnyUrl('ftp://foo.bar', scheme='ftp', host='foo.bar', tld='bar', host_type='domain') port=1111 db=DBConfig(user='admin', password=SecretStr('**********'))
"""

print(cfg.json(indent=2))
"""
{
  "host": "ftp://foo.bar",
  "port": 1111,
  "db": {
    "user": "admin",
    "password": "**********"
  }
}
"""

# every model has a schema, too!
print(cfg.db.schema_json(indent=2))
"""
{
  "title": "DBConfig",
  "description": "Base class, parent of every config class. Internally wraps :class:`BaseModel`of\npydantic and behaves transparent except for two cases:\n\n- If the constructor gets `config_sources` as kwarg, these sources are used as\n  input to enrich the other kwargs.\n- If the class has the class variable `CONFIG_SOURCES` defined, these sources are\n  used as input.\n\nIn the latter case, a singleton mechanism is activated, returning the same config\nclass instance every time the constructor is called.",
  "type": "object",
  "properties": {
    "user": {
      "title": "User",
      "type": "string"
    },
    "password": {
      "title": "Password",
      "type": "string",
      "writeOnly": true,
      "format": "password"
    }
  },
  "required": [
    "user",
    "password"
  ]
}
"""
