| {{ table | first | join (' | ')}} |  
| {%- for e in table[1] -%} 
{{ e }} |
{%- endfor %}  
{% for line in table[2:] -%}
| {{ line | join (' | ')}} |
{% endfor %}
