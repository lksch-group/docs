In der Gruppenarbeit wollen wir folgende **Kompetenzen** erwerben:

{% for competence in competences %}
{{ loop.index + 1 }}. {{ competence }}
{% endfor %}